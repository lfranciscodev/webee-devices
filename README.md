# Webee.io Challenge 
### Objetive ###

* Expose a "Device" resource with their respective endpoints
* [Repository](https://bitbucket.org/lfranciscodev/webee-devices)

### Technologies ###

* JDK 11
* Maven 3.6.3
* SpringBoot 2.3.6.RELEASE
* Google Cloud Datastore (App key was added to resources for demonstration purposes).
* Google APP Engine
### Dependencies ###

* SpringBoot Starter Web (https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-web)
* SpringBoot Starter Test (https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-test)
* Spring Cloud GCP Starter Config (https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-gcp-starter-config/1.0.0.M3)
* Spring Cloud GCP Datastore (https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-gcp-starter-data-datastore/1.1.0.M2)
* Lombok (https://projectlombok.org)

### Endpoints ###

* Make sure to send the Accept "application/json" header
* All endpoints keep "api/devices" prefix. Example: http://127.0.0.1:8080/api/devices/delete/5180000076562432

* [Method GET] /readAll (Returns all devices).
* [Method GET] /find/{id} (Returns the specified device by id).
* [Method GET] /findMacAddress/{macAddress} (Returns the specified device by MAC Address).
* [Method DELETE] /delete/{id}  (Remove the specified device).
* [Method POST] /save (Saves the specified device).

* The endpoint "/save" requires a JSON Body with the MAC Address of the device. Note that if the date is before 01/01/2020 or if that mac is being used, it will return a CONFLICT Status code (409).

