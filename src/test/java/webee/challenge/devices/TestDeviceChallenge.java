package webee.challenge.devices;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import webee.challenge.devices.Entity.Device;
import webee.challenge.devices.Helpers.Helpers;
import webee.challenge.devices.Repository.DeviceRepository;

@SpringBootTest
class TestDeviceChallenge {

  String macAddress1 = "A2:5A:EC:AB:AC:FA";
  String macAddress2 = "AE:1C:5A:7A:8C:9F";
  String macAddress3 = "B2:CA:A5:E9:B1:99";
  String macAddress4 = "A2:DA:EE:CB:AA:8D";
  String macAddress5 = "FF:FF:FF:FF:FF:FF";
  String macAddress6 = "FF:33F:RF:AF:FF:FK";
  String macAddress7 = "FF:F1F:FF:FF:FF:FF";
  String macAddress8 = "FF:F12F:FF:FF:1";

  @Autowired
  DeviceRepository deviceRepository;

  @Test
  public void testHelper() throws ParseException {

    System.out.println("Testing MAC Address...");

    System.out.println(macAddress1 + " --> " + Helpers.isValidMACAddress(macAddress1));
    System.out.println(macAddress2 + " --> " + Helpers.isValidMACAddress(macAddress2));
    System.out.println(macAddress3 + " --> " + Helpers.isValidMACAddress(macAddress3));
    System.out.println(macAddress4 + " --> " + Helpers.isValidMACAddress(macAddress4));
    System.out.println(macAddress5 + " --> " + Helpers.isValidMACAddress(macAddress5));
    System.out.println(macAddress6 + " --> " + Helpers.isValidMACAddress(macAddress6));
    System.out.println(macAddress7 + " --> " + Helpers.isValidMACAddress(macAddress7));
    System.out.println(macAddress8 + " --> " + Helpers.isValidMACAddress(macAddress8));
    
    System.out.println("Testing Dates...");
    
    Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse("02/01/2012");
    Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse("01/05/2007");
    Date date3 = new SimpleDateFormat("dd/MM/yyyy").parse("23/02/2021");
    Date date4 = new SimpleDateFormat("dd/MM/yyyy").parse("21/09/2020");

    System.out.println(date1 + " --> " + Helpers.isBefore(date1));
    System.out.println(date2 + " --> " + Helpers.isBefore(date2));
    System.out.println(date3 + " --> " + Helpers.isBefore(date3));
    System.out.println(date4 + " --> " + Helpers.isBefore(date4));
  }

  @Test
  public void testDataSaving() {

    System.out.println("Testing data saving...");

    System.out.println("Deleting all data...");

    deviceRepository.deleteAll();

    System.out.println("Saving data...");

    saveDevices();

  }
      
  private void saveDevices() {

    boolean saved;
    
    System.out.println("Saving device with MAC Address: " + macAddress1);
    deviceRepository.save(new Device(null, macAddress1,Date.from(Instant.now())));
    saved = deviceRepository.findByMacAddress(macAddress1).isEmpty() ? false : true;
    System.out.println("saved: " + saved);

    System.out.println("Saving device with MAC Address: " + macAddress2);
    deviceRepository.save(new Device(null, macAddress2,Date.from(Instant.now())));
    saved = deviceRepository.findByMacAddress(macAddress2).isEmpty() ? false : true;
    System.out.println("saved: " + saved);

    System.out.println("Saving device with MAC Address: " + macAddress3);
    deviceRepository.save(new Device(null, macAddress3,Date.from(Instant.now())));
    saved = deviceRepository.findByMacAddress(macAddress3).isEmpty() ? false : true;
    System.out.println("saved: " + saved);

    System.out.println("Saving device with MAC Address: " + macAddress4);
    deviceRepository.save(new Device(null, macAddress4,Date.from(Instant.now())));
    saved = deviceRepository.findByMacAddress(macAddress4).isEmpty() ? false : true;
    System.out.println("saved: " + saved);

    System.out.println("Saving device with MAC Address: " + macAddress5);
    deviceRepository.save(new Device(null, macAddress5,Date.from(Instant.now())));
    saved = deviceRepository.findByMacAddress(macAddress5).isEmpty() ? false : true;
    System.out.println("saved: " + saved);
  }
}
