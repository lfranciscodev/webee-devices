package webee.challenge.devices.Entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import webee.challenge.devices.DTO.DeviceDTO;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Device implements Serializable {

  @Id
  private Long id;
  private String macAddress;
  @DateTimeFormat(pattern = "yyy")
  private Date timeStamp;

  public Device(DeviceDTO device) {
    this.id = device.getId();
    this.macAddress = device.getMacAddress();
    this.timeStamp = device.getTimeStamp();
  }

}
