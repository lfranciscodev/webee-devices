package webee.challenge.devices.Repository;

import java.util.List;


import org.springframework.cloud.gcp.data.datastore.repository.DatastoreRepository;
import org.springframework.cloud.gcp.data.datastore.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import webee.challenge.devices.Entity.Device;

@Repository
public interface DeviceRepository extends DatastoreRepository<Device, Long> {
  @Query("select * from device where macAddress = @macAddress")
  List<Device> findByMacAddress(@Param("macAddress") String macAddress);
}
