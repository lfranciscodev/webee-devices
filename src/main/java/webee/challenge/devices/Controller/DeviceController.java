package webee.challenge.devices.Controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import webee.challenge.devices.DTO.DeviceDTO;
import webee.challenge.devices.Entity.Device;
import webee.challenge.devices.Helpers.Helpers;
import webee.challenge.devices.Service.DeviceService;

@RequestMapping("/api/devices")
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DeviceController {

  @Autowired
  DeviceService deviceService;

  @GetMapping("/readAll")
  public List<DeviceDTO> readAll() {

    List<Device> deviceList = StreamSupport.stream(deviceService.findAll().spliterator(), false).collect(Collectors.toList());
    List<DeviceDTO> deviceDTOs = new ArrayList<>();
    deviceList.forEach(new Consumer<Device>() {
      @Override
      public void accept(Device device) {
        deviceDTOs.add(new DeviceDTO(device));
      }
    });
    return deviceDTOs;  
  } 

  @GetMapping("/find/{id}")
  public ResponseEntity<?> readById(@PathVariable Long id) {
    Optional<Device> device = deviceService.findById(id);
    if (!deviceService.findById(id).isPresent()) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(new DeviceDTO(device.get()));
  }

  @GetMapping("/findMacAddress/{macAddress}")
  public ResponseEntity<?> readByMacAddress(@PathVariable String macAddress) {
    List<Device> list = deviceService.findByMacAddress(macAddress);
    if (list.isEmpty()) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(new DeviceDTO(list.get(0)));
  }

  @PostMapping("/save")
  public ResponseEntity<?> save(@RequestBody DeviceDTO deviceDTO) throws ParseException {
    List<Device> list = deviceService.findByMacAddress(deviceDTO.getMacAddress());

    if (list.isEmpty() && !Helpers.isBefore(Instant.now().toDate())) {
      deviceDTO.setTimeStamp(Instant.now().toDate());
      return ResponseEntity.status(HttpStatus.CREATED).body(deviceService.save(new Device(deviceDTO)));
    }
    return ResponseEntity.status(HttpStatus.CONFLICT).build();
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity<?> deleteById(@PathVariable Long id) {
    if (!deviceService.findById(id).isPresent()) {
      return ResponseEntity.notFound().build();
    }
    deviceService.deleteById(id);
    return ResponseEntity.ok().build();
  }
}
