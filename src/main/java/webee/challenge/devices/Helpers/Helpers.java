package webee.challenge.devices.Helpers;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.*;

/**
 * This class conatin the logic to validate a MAC Address.
 * Format: FF:FF:FF:FF:FF:FF
 */

public class Helpers {   

        public static boolean isValidMACAddress(String str){
       String regex = "^([0-9A-Fa-f]{2}[:]){5}([0-9A-Fa-f]{2})$";
        Pattern p = Pattern.compile(regex);
        if (str == null || str.isEmpty())
            return false;
        Matcher m = p.matcher(str);
        return m.matches();
    }

    public static boolean isBefore(Date date) throws ParseException{
        
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date d = format.parse("01/01/2020");
        return (date.before(d));
    }
}