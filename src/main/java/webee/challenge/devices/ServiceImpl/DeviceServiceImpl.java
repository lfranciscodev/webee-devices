package webee.challenge.devices.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import webee.challenge.devices.Entity.Device;
import webee.challenge.devices.Repository.DeviceRepository;
import webee.challenge.devices.Service.DeviceService;

@Service
public class DeviceServiceImpl implements DeviceService {

  @Autowired
  DeviceRepository deviceRepository;

  @Override
  @Transactional(readOnly = true)
  public Iterable<Device> findAll() {
    return deviceRepository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public List<Device> findByMacAddress(String macAddress) {
    return deviceRepository.findByMacAddress(macAddress);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<Device> findById(Long id) {
    return deviceRepository.findById(id);
  }

  @Override
  @Transactional
  public Device save(Device device) {
    return deviceRepository.save(device);
  }

  @Override
  public void deleteById(long id) {
    deviceRepository.deleteById(id);
  }

}
