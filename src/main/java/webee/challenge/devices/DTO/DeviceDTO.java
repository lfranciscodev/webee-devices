package webee.challenge.devices.DTO;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import webee.challenge.devices.Entity.Device;

@Data 
@AllArgsConstructor @NoArgsConstructor
public class DeviceDTO {
  
  private Long id;
  private String macAddress;
  @DateTimeFormat(pattern = "yyy")
  private Date timeStamp;

  public DeviceDTO(Device device) {
    this.id = device.getId();
    this.macAddress = device.getMacAddress();
    this.timeStamp = device.getTimeStamp();
  }
 
}
