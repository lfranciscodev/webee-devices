package webee.challenge.devices.Service;

import java.util.List;
import java.util.Optional;

import webee.challenge.devices.Entity.Device;


public interface DeviceService  {
  
  public Iterable<Device> findAll();
  public Optional<Device> findById(Long id);
  public List<Device> findByMacAddress(String macAddress);
  public Device save(Device device);
  public void deleteById(long id);
  
}
